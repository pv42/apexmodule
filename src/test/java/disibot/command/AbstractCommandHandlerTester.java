package disibot.command;

import disibot.util.Command;
import fakejda.FakeGuild;
import fakejda.FakeMember;
import fakejda.FakeMessage;
import fakejda.FakeMessageChannel;
import fakejda.FakeMessageEvent;
import fakejda.FakeTextChannel;
import fakejda.FakeUser;

import javax.annotation.RegEx;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public abstract class AbstractCommandHandlerTester<T extends CommandHandler> {
    protected final FakeGuild guild = new FakeGuild("FG7",2);
    protected final FakeMessageChannel channel = new FakeTextChannel(guild);
    protected final FakeUser user = new FakeUser(420069);
    protected final FakeMember member = new FakeMember(guild, user);
    protected T handler;

    protected static void assertLastMessage(String expectedLastMessage, FakeMessageChannel channel) {
        assertTrue(channel.getMessages().size() > 0);
        assertEquals(expectedLastMessage, channel.getMessages().get(channel.getMessages().size() - 1).getContentRaw());
    }

    protected void assertLastMessage(String expectedLastMessage) {
        assertLastMessage(expectedLastMessage, channel);
    }

    protected void assertLastMessageMatch(@RegEx String expectedLastMessageRegex) {
        assertTrue(channel.getMessages().size() > 0);
        assertStringMatch(expectedLastMessageRegex, channel.getMessages().get(channel.getMessages().size() - 1).getContentRaw());
    }

    private static void assertStringMatch(String regexExpected, String actual) {
        if(!actual.matches(regexExpected)) {
            assertEquals(regexExpected, actual); // this is a hacky workaround to display the expected/actual values
            fail(); // failsave in case regex does not match but string does
        }
    }

    protected void sendCommand(String command) {
        FakeMessage message = new FakeMessage("!" + command, channel);
        handler.handleCommand(new Command(command), new FakeMessageEvent(message, member));
    }
}
