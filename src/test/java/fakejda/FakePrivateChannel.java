package fakejda;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.requests.RestAction;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;

public class FakePrivateChannel extends FakeMessageChannel implements PrivateChannel {
    @Nonnull
    @Override
    public User getUser() {
        return null;
    }

    @Override
    public boolean hasLatestMessage() {
        return false;
    }

    @Override
    public boolean canTalk() {
        return false;
    }

    @Nonnull
    @Override
    public String getName() {
        return null;
    }

    @Nonnull
    @Override
    public ChannelType getType() {
        return ChannelType.PRIVATE;
    }

    @Nonnull
    @Override
    public JDA getJDA() {
        return null;
    }

    @NotNull
    @Override
    public RestAction<Void> delete() {
        return null;
    }

    @Override
    public long getIdLong() {
        return 0;
    }
}
