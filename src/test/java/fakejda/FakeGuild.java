package fakejda;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Region;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.entities.templates.Template;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import net.dv8tion.jda.api.interactions.commands.Command;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.privileges.CommandPrivilege;
import net.dv8tion.jda.api.managers.AudioManager;
import net.dv8tion.jda.api.managers.GuildManager;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.AuditableRestAction;
import net.dv8tion.jda.api.requests.restaction.ChannelAction;
import net.dv8tion.jda.api.requests.restaction.CommandEditAction;
import net.dv8tion.jda.api.requests.restaction.CommandListUpdateAction;
import net.dv8tion.jda.api.requests.restaction.MemberAction;
import net.dv8tion.jda.api.requests.restaction.RoleAction;
import net.dv8tion.jda.api.requests.restaction.order.CategoryOrderAction;
import net.dv8tion.jda.api.requests.restaction.order.ChannelOrderAction;
import net.dv8tion.jda.api.requests.restaction.order.RoleOrderAction;
import net.dv8tion.jda.api.requests.restaction.pagination.AuditLogPaginationAction;
import net.dv8tion.jda.api.utils.cache.MemberCacheView;
import net.dv8tion.jda.api.utils.cache.SnowflakeCacheView;
import net.dv8tion.jda.api.utils.cache.SortedSnowflakeCacheView;
import net.dv8tion.jda.api.utils.concurrent.Task;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.temporal.TemporalAccessor;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

public class FakeGuild implements Guild {
    private final String name;
    private final List<FakeTextChannel> channels = new ArrayList<>();
    private Member owner;
    private final List<Member> members = new ArrayList<>();
    private final int id;
    private final List<Role> roles = new ArrayList<>();
    private Member selfMember;
    private FakeJDA jda;
    public FakeGuild(String name) {
        this.name = name;
        id = 0;
    }

    public FakeGuild(FakeJDA jda, String name) {
        this.jda = jda;
        this.name = name;
        this.id = 0;
    }

    public FakeGuild(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public FakeGuild(FakeJDA jda, String name, int id) {
        this.jda = jda;
        this.name = name;
        this.id = id;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }

    public void addChannel(FakeTextChannel channel) {
        channels.add(channel);
    }

    public void setSelfMember(Member selfMember) {
        this.selfMember = selfMember;
    }

    public void addMember(FakeUser user) {
        members.add(new FakeMember(this, user));
    }

    public void addMember(FakeMember member) {
        members.add(member);
    }

    @Nonnull
    @Override
    public List<Member> getMembers() {
        return new ArrayList<>(members);
    }

    @NotNull
    @Override
    public RestAction<List<Command>> retrieveCommands() {
        return null;
    }

    @NotNull
    @Override
    public RestAction<Command> retrieveCommandById(@NotNull String id) {
        return null;
    }

    @NotNull
    @Override
    public RestAction<Command> upsertCommand(@NotNull CommandData command) {
        return null;
    }

    @NotNull
    @Override
    public CommandListUpdateAction updateCommands() {
        return null;
    }

    @NotNull
    @Override
    public CommandEditAction editCommandById(@NotNull String id) {
        return null;
    }

    @NotNull
    @Override
    public RestAction<Void> deleteCommandById(@NotNull String commandId) {
        return null;
    }

    @NotNull
    @Override
    public RestAction<List<CommandPrivilege>> retrieveCommandPrivilegesById(@NotNull String commandId) {
        return null;
    }

    @NotNull
    @Override
    public RestAction<Map<String, List<CommandPrivilege>>> retrieveCommandPrivileges() {
        return null;
    }

    @NotNull
    @Override
    public RestAction<List<CommandPrivilege>> updateCommandPrivilegesById(@NotNull String id, @NotNull Collection<? extends CommandPrivilege> privileges) {
        return null;
    }

    @NotNull
    @Override
    public RestAction<Map<String, List<CommandPrivilege>>> updateCommandPrivileges(@NotNull Map<String, ? extends Collection<CommandPrivilege>> privileges) {
        return null;
    }

    @Nonnull
    @Override
    public RestAction<EnumSet<Region>> retrieveRegions(boolean includeDeprecated) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public MemberAction addMember(@Nonnull String accessToken, @Nonnull String userId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isLoaded() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void pruneMemberCache() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean unloadMember(long userId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getMemberCount() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public String getName() {
        return name;
    }

    @Nullable
    @Override
    public String getIconId() {
        return null;
    }

    @Nonnull
    @Override
    public Set<String> getFeatures() {
        throw new UnsupportedOperationException();
    }

    @Nullable
    @Override
    public String getSplashId() {
        throw new UnsupportedOperationException();
    }

    @Nullable
    @Override
    public String getVanityCode() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<VanityInvite> retrieveVanityInvite() {
        throw new UnsupportedOperationException();
    }

    @Nullable
    @Override
    public String getDescription() {
        return null;
    }

    @Nonnull
    @Override
    public Locale getLocale() {
        return null;
    }

    @Nullable
    @Override
    public String getBannerId() {
        return null;
    }

    @Nonnull
    @Override
    public BoostTier getBoostTier() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getBoostCount() {
        return 0;
    }

    @Nonnull
    @Override
    public List<Member> getBoosters() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getMaxMembers() {
        return 0;
    }

    @Override
    public int getMaxPresences() {
        return 0;
    }

    @Nonnull
    @Override
    public RestAction<MetaData> retrieveMetaData() {
        return null;
    }

    @Nullable
    @Override
    public VoiceChannel getAfkChannel() {
        return null;
    }

    @Nullable
    @Override
    public TextChannel getSystemChannel() {
        return null;
    }

    @Nullable
    @Override
    public TextChannel getRulesChannel() {
        return null;
    }

    @Nullable
    @Override
    public TextChannel getCommunityUpdatesChannel() {
        return null;
    }

    @Nullable
    @Override
    public Member getOwner() {
        return owner;
    }

    @Override
    public long getOwnerIdLong() {
        if(owner == null) return 0;
        return owner.getIdLong();
    }

    @Nonnull
    @Override
    public Timeout getAfkTimeout() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isMember(@Nonnull User user) {
        return true;
    }

    @Nonnull
    @Override
    public Member getSelfMember() {
        if(selfMember != null) return selfMember;
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public NSFWLevel getNSFWLevel() {
        return null;
    }

    @Nullable
    @Override
    public Member getMember(@Nonnull User user) {
        Member member = new FakeMember(this, user);
        members.add(member);
        return member;
    }

    @Nonnull
    @Override
    public MemberCacheView getMemberCache() {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public SortedSnowflakeCacheView<StageChannel> getStageChannelCache() {
        return null;
    }

    @NotNull
    @Override
    public SortedSnowflakeCacheView<ThreadChannel> getThreadChannelCache() {
        return null;
    }

    @Nonnull
    @Override
    public SortedSnowflakeCacheView<Category> getCategoryCache() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public SortedSnowflakeCacheView<StoreChannel> getStoreChannelCache() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public SortedSnowflakeCacheView<TextChannel> getTextChannelCache() {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public SortedSnowflakeCacheView<NewsChannel> getNewsChannelCache() {
        return null;
    }

    @Nonnull
    @Override
    public SortedSnowflakeCacheView<VoiceChannel> getVoiceChannelCache() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public List<GuildChannel> getChannels(boolean includeHidden) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public SortedSnowflakeCacheView<Role> getRoleCache() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public SnowflakeCacheView<Emote> getEmoteCache() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<List<ListedEmote>> retrieveEmotes() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<ListedEmote> retrieveEmoteById(@Nonnull String id) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<List<Ban>> retrieveBanList() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<Ban> retrieveBanById(@Nonnull String userId) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<Integer> retrievePrunableMemberCount(int days) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public Role getPublicRole() {
        throw new UnsupportedOperationException();
    }

    @Nullable
    @Override
    public TextChannel getDefaultChannel() {
        if(channels.size()>0) return channels.get(0);
        return null;
    }

    @Nonnull
    @Override
    public GuildManager getManager() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isBoostProgressBarEnabled() {
        return false;
    }

    @Nonnull
    @Override
    public AuditLogPaginationAction retrieveAuditLogs() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<Void> leave() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<Void> delete() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<Void> delete(@Nullable String mfaCode) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public AudioManager getAudioManager() {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public Task<Void> requestToSpeak() {
        return null;
    }

    @NotNull
    @Override
    public Task<Void> cancelRequestToSpeak() {
        return null;
    }

    @Nonnull // may be null
    @Override
    public JDA getJDA() {
        return jda;
    }

    @Nonnull
    @Override
    public RestAction<List<Invite>> retrieveInvites() {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public RestAction<List<Template>> retrieveTemplates() {
        return null;
    }

    @NotNull
    @Override
    public RestAction<Template> createTemplate(@NotNull String name, @org.jetbrains.annotations.Nullable String description) {
        return null;
    }

    @Nonnull
    @Override
    public RestAction<List<Webhook>> retrieveWebhooks() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public List<GuildVoiceState> getVoiceStates() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public VerificationLevel getVerificationLevel() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public NotificationLevel getDefaultNotificationLevel() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public MFALevel getRequiredMFALevel() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public ExplicitContentLevel getExplicitContentLevel() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public Task<Void> loadMembers(@Nonnull Consumer<Member> callback) {
        return null;
    }

    @Nonnull
    @Override
    public RestAction<Member> retrieveMemberById(long id, boolean update) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public Task<List<Member>> retrieveMembersByIds(boolean includePresence, @Nonnull long... ids) {
        return null;
    }

    @Nonnull
    @Override
    public Task<List<Member>> retrieveMembersByPrefix(@Nonnull String prefix, int limit) {
        return null;
    }

    @NotNull
    @Override
    public RestAction<List<ThreadChannel>> retrieveActiveThreads() {
        return null;
    }

    @NotNull
    @Override
    public RestAction<Void> moveVoiceMember(@NotNull Member member, @org.jetbrains.annotations.Nullable AudioChannel audioChannel) {
        return null;
    }

    @Nonnull
    @Override
    public AuditableRestAction<Void> modifyNickname(@Nonnull Member member, @Nullable String nickname) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public AuditableRestAction<Integer> prune(int days, boolean wait, @Nonnull Role... roles) {
        return null;
    }

    @Nonnull
    @Override
    public AuditableRestAction<Void> kick(@Nonnull Member member, @Nullable String reason) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public AuditableRestAction<Void> kick(@Nonnull String userId, @Nullable String reason) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public AuditableRestAction<Void> ban(@Nonnull User user, int delDays, @Nullable String reason) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public AuditableRestAction<Void> ban(@Nonnull String userId, int delDays, @Nullable String reason) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public AuditableRestAction<Void> unban(@Nonnull String userId) {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public AuditableRestAction<Void> timeoutUntilById(@NotNull String userId, @NotNull TemporalAccessor temporal) {
        return null;
    }

    @NotNull
    @Override
    public AuditableRestAction<Void> removeTimeoutById(@NotNull String userId) {
        return null;
    }

    @Nonnull
    @Override
    public AuditableRestAction<Void> deafen(@Nonnull Member member, boolean deafen) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public AuditableRestAction<Void> mute(@Nonnull Member member, boolean mute) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public AuditableRestAction<Void> addRoleToMember(@Nonnull Member member, @Nonnull Role role) {
        return new AuditableRestAction<>() {
            @NotNull
            @Override
            public AuditableRestAction<Void> reason(@Nullable String reason) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public AuditableRestAction<Void> setCheck(@Nullable BooleanSupplier checks) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public AuditableRestAction<Void> timeout(long timeout, @NotNull TimeUnit unit) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public AuditableRestAction<Void> deadline(long timestamp) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public JDA getJDA() {
                throw new UnsupportedOperationException();
            }

            @Override
            public void queue(@org.jetbrains.annotations.Nullable Consumer<? super Void> success, @org.jetbrains.annotations.Nullable Consumer<? super Throwable> failure) {
                throw new UnsupportedOperationException();
            }

            @Override
            public Void complete(boolean shouldQueue) throws RateLimitedException {
                FakeRole fakeRole = (FakeRole) role;
                ((FakeMember)member).addRole(fakeRole);
                return null;
            }

            @NotNull
            @Override
            public CompletableFuture<Void> submit(boolean shouldQueue) {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Nonnull
    @Override
    public AuditableRestAction<Void> removeRoleFromMember(@Nonnull Member member, @Nonnull Role role) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public AuditableRestAction<Void> modifyMemberRoles(@Nonnull Member member, @Nullable Collection<Role> rolesToAdd, @Nullable Collection<Role> rolesToRemove) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public AuditableRestAction<Void> modifyMemberRoles(@Nonnull Member member, @Nonnull Collection<Role> roles) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public AuditableRestAction<Void> transferOwnership(@Nonnull Member newOwner) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public ChannelAction<TextChannel> createTextChannel(@Nonnull String name) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public ChannelAction<TextChannel> createTextChannel(@Nonnull String name, @Nullable Category parent) {
        return null;
    }

    @NotNull
    @Override
    public ChannelAction<NewsChannel> createNewsChannel(@NotNull String name, @org.jetbrains.annotations.Nullable Category parent) {
        return null;
    }

    @Nonnull
    @Override
    public ChannelAction<VoiceChannel> createVoiceChannel(@Nonnull String name) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public ChannelAction<VoiceChannel> createVoiceChannel(@Nonnull String name, @Nullable Category parent) {
        return null;
    }

    @NotNull
    @Override
    public ChannelAction<StageChannel> createStageChannel(@NotNull String name, @org.jetbrains.annotations.Nullable Category parent) {
        return null;
    }

    @Nonnull
    @Override
    public ChannelAction<Category> createCategory(@Nonnull String name) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RoleAction createRole() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public AuditableRestAction<Emote> createEmote(@Nonnull String name, @Nonnull Icon icon, @Nonnull Role... roles) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public ChannelOrderAction modifyCategoryPositions() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public ChannelOrderAction modifyTextChannelPositions() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public ChannelOrderAction modifyVoiceChannelPositions() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public CategoryOrderAction modifyTextChannelPositions(@Nonnull Category category) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public CategoryOrderAction modifyVoiceChannelPositions(@Nonnull Category category) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RoleOrderAction modifyRolePositions(boolean useAscendingOrder) {
        throw new UnsupportedOperationException();
    }

    @Override
    public long getIdLong() {
        return id;
    }

    public void setOwner(FakeMember owner) {
        this.owner = owner;
    }

    public void addRole(FakeRole role) {
        roles.add(role);
    }
}
