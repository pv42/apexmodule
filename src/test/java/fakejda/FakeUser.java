package fakejda;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.requests.RestAction;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class FakeUser implements User {
    private long id;
    private String name;

    public FakeUser(long id) {
        this.id = id;
        name = "FakeUser";
    }

    @Nonnull
    @Override
    public String getName() {
        return name;
    }

    @Nonnull
    @Override
    public String getDiscriminator() {
        return "DiscriminatorString";
    }

    @Nullable
    @Override
    public String getAvatarId() {
        return null;
    }

    @Nonnull
    @Override
    public String getDefaultAvatarId() {
        return "avatarID";
    }

    @NotNull
    @Override
    public RestAction<Profile> retrieveProfile() {
        return null;
    }

    @Nonnull
    @Override
    public String getAsTag() {
        return "@FakeUser";
    }

    @Override
    public boolean hasPrivateChannel() {
        return false;
    }

    @Nonnull
    @Override
    public RestAction<PrivateChannel> openPrivateChannel() {
        return null;
    }

    @Nonnull
    @Override
    public List<Guild> getMutualGuilds() {
        return new ArrayList<>();
    }

    @Override
    public boolean isBot() {
        return false;
    }

    @Override
    public boolean isSystem() {
        return false;
    }

    @Nonnull
    @Override
    public JDA getJDA() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public EnumSet<UserFlag> getFlags() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getFlagsRaw() {
        return 0;
    }

    @Nonnull
    @Override
    public String getAsMention() {
        return "@FakeUser";
    }

    @Override
    public long getIdLong() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == this) return true;
        if(!(obj instanceof FakeUser)) return false;
        return this.id == ((FakeUser) obj).getIdLong();
    }

    public void setName(String name) {
        this.name = name;
    }
}
