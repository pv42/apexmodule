package fakejda;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import net.dv8tion.jda.api.managers.channel.concrete.TextChannelManager;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.AuditableRestAction;
import net.dv8tion.jda.api.requests.restaction.ChannelAction;
import net.dv8tion.jda.api.requests.restaction.InviteAction;
import net.dv8tion.jda.api.requests.restaction.PermissionOverrideAction;
import net.dv8tion.jda.api.requests.restaction.ThreadChannelAction;
import net.dv8tion.jda.api.requests.restaction.WebhookAction;
import net.dv8tion.jda.api.requests.restaction.pagination.ThreadChannelPaginationAction;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

public class FakeTextChannel extends FakeMessageChannel implements TextChannel {

    private final FakeGuild guild;
    private final JDA jda;

    public FakeTextChannel() {
        guild = null;
        jda = null;
    }


    public FakeTextChannel(FakeGuild guild) {
        this.guild = guild;
        this.jda = guild.getJDA();
    }


    @Override
    public long getLatestMessageIdLong() {
        return 0;
    }

    @Override
    public boolean hasLatestMessage() {
        return false;
    }

    @Nonnull
    @Override
    public String getName() {
        return  "FakeMessageChannel";
    }

    @Nonnull
    @Override
    public Guild getGuild() {
        if (guild == null) throw new UnsupportedOperationException();
        return guild;
    }

    @Nonnull
    @Override
    public List<Member> getMembers() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getPosition() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getPositionRaw() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public ChannelType getType() {
        return ChannelType.TEXT;
    }

    @Nonnull
    @Override
    public JDA getJDA() {
        if(jda != null) return jda;
        throw new UnsupportedOperationException();
    }

    @Nullable
    @Override
    public PermissionOverride getPermissionOverride(@Nonnull IPermissionHolder permissionHolder) {
        return null;
    }

    @Nonnull
    @Override
    public List<PermissionOverride> getPermissionOverrides() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public List<PermissionOverride> getMemberPermissionOverrides() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public List<PermissionOverride> getRolePermissionOverrides() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isSynced() {
        return false;
    }

    @Override
    public long getIdLong() {
        return 0;
    }


    @Nullable
    @Override
    public String getTopic() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isNSFW() {
        return false;
    }

    @Override
    public int getSlowmode() {
        return 0;
    }

    @Nonnull
    @Override
    public ChannelAction<TextChannel> createCopy(@Nonnull Guild guild) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public ChannelAction<TextChannel> createCopy() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public TextChannelManager getManager() {
        throw new UnsupportedOperationException();
    }

    @Override
    public long getParentCategoryIdLong() {
        return 0;
    }

    @Nonnull
    @Override
    public AuditableRestAction<Void> delete() {
        throw new UnsupportedOperationException();
    }

    @Override
    public IPermissionContainer getPermissionContainer() {
        return null;
    }

    @Nonnull
    @Override
    public PermissionOverrideAction createPermissionOverride(@Nonnull IPermissionHolder permissionHolder) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public PermissionOverrideAction putPermissionOverride(@Nonnull IPermissionHolder permissionHolder) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public InviteAction createInvite() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<List<Invite>> retrieveInvites() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<List<Webhook>> retrieveWebhooks() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public WebhookAction createWebhook(@Nonnull String name) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<Void> deleteMessages(@Nonnull Collection<Message> messages) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<Void> deleteMessagesByIds(@Nonnull Collection<String> messageIds) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public AuditableRestAction<Void> deleteWebhookById(@Nonnull String id) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<Void> clearReactionsById(@Nonnull String messageId) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<Void> clearReactionsById(@Nonnull String messageId, @Nonnull String unicode) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<Void> clearReactionsById(@Nonnull String messageId, @Nonnull Emote emote) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<Void> removeReactionById(@Nonnull String messageId, @Nonnull String unicode, @Nonnull User user) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean canTalk() {
        return false;
    }

    @Override
    public boolean canTalk(@Nonnull Member member) {
        return false;
    }

    @Override
    public int compareTo(@NotNull GuildChannel o) {
        return 0;
    }

    @Nonnull
    @Override
    public String getAsMention() {
        throw new UnsupportedOperationException();
    }

    @Override
    public MessageHistory getHistory() {
        return new MessageHistory(this);
    }

    @Nonnull
    @Override
    public RestAction<Message> retrieveMessageById(long messageId) {
        return new RestAction<>() {
            @Nonnull
            @Override
            public JDA getJDA() {
                throw new UnsupportedOperationException();
            }

            @Nonnull
            @Override
            public RestAction<Message> setCheck(@Nullable BooleanSupplier checks) {
                throw new UnsupportedOperationException();
            }

            @Override
            public void queue(@Nullable Consumer<? super Message> success, @Nullable Consumer<? super Throwable> failure) {
                throw new UnsupportedOperationException();
            }

            @Override
            public Message complete(boolean shouldQueue) throws RateLimitedException {
                for(Message msg: messages) {
                    if(msg.getIdLong() == messageId)
                        return msg;
                }
                return null;
            }

            @Nonnull
            @Override
            public CompletableFuture<Message> submit(boolean shouldQueue) {
                throw new UnsupportedOperationException();
            }
        };
    }

    @NotNull
    @Override
    public ThreadChannelAction createThreadChannel(String name, boolean isPrivate) {
        return null;
    }

    @NotNull
    @Override
    public ThreadChannelAction createThreadChannel(String name, long messageId) {
        return null;
    }

    @NotNull
    @Override
    public ThreadChannelPaginationAction retrieveArchivedPublicThreadChannels() {
        return null;
    }

    @NotNull
    @Override
    public ThreadChannelPaginationAction retrieveArchivedPrivateThreadChannels() {
        return null;
    }

    @NotNull
    @Override
    public ThreadChannelPaginationAction retrieveArchivedPrivateJoinedThreadChannels() {
        return null;
    }
}
