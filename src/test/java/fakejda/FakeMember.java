package fakejda;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.ClientType;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.GuildChannel;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.IPermissionContainer;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.awt.Color;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;

public class FakeMember implements Member {
    private Guild guild;
    private User user;
    private List<Role> roles = new ArrayList<>();

    public FakeMember(FakeGuild guild, User user) {
        this.guild = guild;
        this.user = user;
        guild.addMember(this);
    }

    public void addRole(FakeRole role) {
        roles.add(role);
    }

    @Nonnull
    @Override
    public User getUser() {
        return user;
    }

    @Nonnull
    @Override
    public Guild getGuild() {
        return guild;
    }

    @Nonnull
    @Override
    public EnumSet<Permission> getPermissions() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public EnumSet<Permission> getPermissions(@Nonnull GuildChannel channel) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public EnumSet<Permission> getPermissionsExplicit() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public EnumSet<Permission> getPermissionsExplicit(@Nonnull GuildChannel channel) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasPermission(@Nonnull Permission... permissions) {
        return false;
    }

    @Override
    public boolean hasPermission(@Nonnull Collection<Permission> permissions) {
        return false;
    }

    @Override
    public boolean hasPermission(@Nonnull GuildChannel channel, @Nonnull Permission... permissions) {
        return true;
    }

    @Override
    public boolean hasPermission(@Nonnull GuildChannel channel, @Nonnull Collection<Permission> permissions) {
        return false;
    }

    @Override
    public boolean canSync(@NotNull IPermissionContainer targetChannel, @NotNull IPermissionContainer syncSource) {
        return false;
    }

    @Override
    public boolean canSync(@NotNull IPermissionContainer channel) {
        return false;
    }

    @Nonnull
    @Override
    public JDA getJDA() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public OffsetDateTime getTimeJoined() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasTimeJoined() {
        throw new UnsupportedOperationException();
    }

    @Nullable
    @Override
    public OffsetDateTime getTimeBoosted() {
        throw new UnsupportedOperationException();
    }

    @org.jetbrains.annotations.Nullable
    @Override
    public OffsetDateTime getTimeOutEnd() {
        return null;
    }

    @Nullable
    @Override
    public GuildVoiceState getVoiceState() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public List<Activity> getActivities() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public OnlineStatus getOnlineStatus() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public OnlineStatus getOnlineStatus(@Nonnull ClientType type) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public EnumSet<ClientType> getActiveClients() {
        throw new UnsupportedOperationException();
    }

    @Nullable
    @Override
    public String getNickname() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public String getEffectiveName() {
        throw new UnsupportedOperationException();
    }

    @org.jetbrains.annotations.Nullable
    @Override
    public String getAvatarId() {
        return null;
    }

    @Nonnull
    @Override
    public List<Role> getRoles() {
        return roles;
    }

    @Nullable
    @Override
    public Color getColor() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getColorRaw() {
        return -1;
    }

    @Override
    public boolean canInteract(@Nonnull Member member) {
        return false;
    }

    @Override
    public boolean canInteract(@Nonnull Role role) {
        return false;
    }

    @Override
    public boolean canInteract(@Nonnull Emote emote) {
        return false;
    }

    @Override
    public boolean isOwner() {
        return false;
    }

    @Override
    public boolean isPending() {
        return false;
    }

    @Nullable
    @Override
    public TextChannel getDefaultChannel() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public String getAsMention() {
        throw new UnsupportedOperationException();
    }

    @Override
    public long getIdLong() {
        return user == null ? -100 : user.getIdLong() + guild.getIdLong();
    }
}
