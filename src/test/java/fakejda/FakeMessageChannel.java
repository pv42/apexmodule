package fakejda;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import net.dv8tion.jda.api.utils.AttachmentOption;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public abstract class FakeMessageChannel implements MessageChannel {

    List<Message> messages = new ArrayList<>();
    List<String> fileContents = new ArrayList<>();
    List<MessageEmbed> embeds = new ArrayList<>();

    @Override
    public long getLatestMessageIdLong() {
        return messages.get(messages.size() - 1).getIdLong();
    }

    @Override
    public boolean hasLatestMessage() {
        return false;
    }

    @Nonnull
    @Override
    public MessageAction sendMessage(@Nonnull CharSequence text) {
        return new FakeMessageAction(() -> messages.add(new FakeMessage((String) text, this)));
    }

    @Nonnull
    @Override
    public MessageAction sendMessageFormat(@Nonnull String format, @Nonnull Object... args) {
        throw new UnsupportedOperationException();
    }


    @Nonnull
    @Override
    public MessageAction sendMessage(@Nonnull Message msg) {
        return new FakeMessageAction(() -> messages.add(msg));
    }

    @Nonnull
    @Override
    public MessageAction sendFile(@Nonnull File file, @Nonnull AttachmentOption... options) {
        return new FakeMessageAction(() -> {
        });
    }

    @Nonnull
    @Override
    public MessageAction sendFile(@Nonnull File file, @Nonnull String fileName, @Nonnull AttachmentOption... options) {
        return new FakeMessageAction(() -> {
            throw new UnsupportedOperationException();
        });
    }

    @Nonnull
    @Override
    public MessageAction sendFile(@Nonnull InputStream data, @Nonnull String fileName, @Nonnull AttachmentOption... options) {
        return new FakeMessageAction(() -> {
            throw new UnsupportedOperationException();
        });
    }

    @Nonnull
    @Override
    public MessageAction sendFile(@Nonnull byte[] data, @Nonnull String fileName, @Nonnull AttachmentOption... options) {
        return new FakeMessageAction(() -> {
            fileContents.add(new String(data));
        });
    }

    public List<Message> getMessages() {
        return messages;
    }

    public List<String> getFiles() {
        return fileContents;
    }

    public List<MessageEmbed> getEmbeds() {
        return embeds;
    }
}
