package fakejda;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Category;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.GuildMessageChannel;
import net.dv8tion.jda.api.entities.IMentionable;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageActivity;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.MessageReaction;
import net.dv8tion.jda.api.entities.MessageReference;
import net.dv8tion.jda.api.entities.MessageSticker;
import net.dv8tion.jda.api.entities.MessageType;
import net.dv8tion.jda.api.entities.NewsChannel;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.ThreadChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import net.dv8tion.jda.api.interactions.components.ActionRow;
import net.dv8tion.jda.api.interactions.components.LayoutComponent;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.AuditableRestAction;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import net.dv8tion.jda.api.requests.restaction.pagination.ReactionPaginationAction;
import org.apache.commons.collections4.Bag;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Formatter;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FakeMessage implements Message {
    private String content;
    private MessageChannel channel;
    private User user;
    private List<MessageReaction> reactions = new ArrayList<>();

    public FakeMessage(String content, MessageChannel channel) {
        this.content = content;
        this.channel = channel;
    }

    public FakeMessage(String content, MessageChannel channel, User sender) {
        this.content = content;
        this.channel = channel;
        this.user = sender;
    }

    @org.jetbrains.annotations.Nullable
    @Override
    public MessageReference getMessageReference() {
        return null;
    }

    @Nullable
    @Override
    public Message getReferencedMessage() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public List<User> getMentionedUsers() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public Bag<User> getMentionedUsersBag() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public List<TextChannel> getMentionedChannels() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public Bag<TextChannel> getMentionedChannelsBag() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public List<Role> getMentionedRoles() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public Bag<Role> getMentionedRolesBag() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public List<Member> getMentionedMembers(@Nonnull Guild guild) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public List<Member> getMentionedMembers() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public List<IMentionable> getMentions(@Nonnull MentionType... types) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isMentioned(@Nonnull IMentionable mentionable, @Nonnull MentionType... types) {
        return false;
    }

    @Override
    public boolean mentionsEveryone() {
        return false;
    }

    @Override
    public boolean isEdited() {
        return false;
    }

    @Nullable
    @Override
    public OffsetDateTime getTimeEdited() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public User getAuthor() {
        return user;
    }

    @Nullable
    @Override
    public Member getMember() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public String getJumpUrl() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public String getContentDisplay() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public String getContentRaw() {
        return content;
    }

    @Nonnull
    @Override
    public String getContentStripped() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public List<String> getInvites() {
        throw new UnsupportedOperationException();
    }

    @Nullable
    @Override
    public String getNonce() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isFromType(@Nonnull ChannelType type) {
        return false;
    }

    @Nonnull
    @Override
    public ChannelType getChannelType() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isWebhookMessage() {
        return false;
    }

    @Nonnull
    @Override
    public MessageChannel getChannel() {
        return channel;
    }

    @NotNull
    @Override
    public GuildMessageChannel getGuildChannel() {
        return null;
    }

    @Nonnull
    @Override
    public PrivateChannel getPrivateChannel() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public TextChannel getTextChannel() {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public NewsChannel getNewsChannel() {
        return null;
    }

    @Nullable
    @Override
    public Category getCategory() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public Guild getGuild() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public List<Attachment> getAttachments() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public List<MessageEmbed> getEmbeds() {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public List<ActionRow> getActionRows() {
        return null;
    }

    @Nonnull
    @Override
    public List<Emote> getEmotes() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public Bag<Emote> getEmotesBag() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public List<MessageReaction> getReactions() {
        return reactions;
    }

    @NotNull
    @Override
    public List<MessageSticker> getStickers() {
        return null;
    }

    @Override
    public boolean isTTS() {
        return false;
    }

    @Nullable
    @Override
    public MessageActivity getActivity() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public MessageAction editMessage(@Nonnull CharSequence newContent) {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public MessageAction editMessageEmbeds(@NotNull Collection<? extends MessageEmbed> embeds) {
        return null;
    }

    @NotNull
    @Override
    public MessageAction editMessageComponents(@NotNull Collection<? extends LayoutComponent> components) {
        return null;
    }

    @Nonnull
    @Override
    public MessageAction editMessageFormat(@Nonnull String format, @Nonnull Object... args) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public MessageAction editMessage(@Nonnull Message newContent) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public AuditableRestAction<Void> delete() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public JDA getJDA() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isPinned() {
        return false;
    }

    @Nonnull
    @Override
    public RestAction<Void> pin() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<Void> unpin() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<Void> addReaction(@Nonnull Emote emote) {
        return new RestAction<>() {
            @Nonnull
            @Override
            public JDA getJDA() {
                throw new UnsupportedOperationException();
            }

            @Nonnull
            @Override
            public RestAction<Void> setCheck(@Nullable BooleanSupplier checks) {
                throw new UnsupportedOperationException();
            }

            @Override
            public void queue(@Nullable Consumer<? super Void> success, @Nullable Consumer<? super Throwable> failure) {
                throw new UnsupportedOperationException();
            }

            @Override
            public Void complete(boolean shouldQueue) throws RateLimitedException {
                MessageReaction.ReactionEmote reactionEmote = mock(MessageReaction.ReactionEmote.class);
                when(reactionEmote.getEmote()).thenReturn(emote);
                reactions.add(new MessageReaction(getChannel(), reactionEmote, getIdLong(), true, 1));
                return null;
            }

            @Nonnull
            @Override
            public CompletableFuture<Void> submit(boolean shouldQueue) {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Nonnull
    @Override
    public RestAction<Void> addReaction(@Nonnull String unicode) {
        return new RestAction<>() {
            @Nonnull
            @Override
            public JDA getJDA() {
                throw new UnsupportedOperationException();
            }

            @Nonnull
            @Override
            public RestAction<Void> setCheck(@Nullable BooleanSupplier checks) {
                throw new UnsupportedOperationException();
            }

            @Override
            public void queue(@Nullable Consumer<? super Void> success, @Nullable Consumer<? super Throwable> failure) {
                throw new UnsupportedOperationException();
            }

            @Override
            public Void complete(boolean shouldQueue) throws RateLimitedException {
                MessageReaction.ReactionEmote reactionEmote = mock(MessageReaction.ReactionEmote.class);
                when(reactionEmote.getEmoji()).thenReturn(unicode);
                reactions.add(new MessageReaction(getChannel(), reactionEmote, getIdLong(), true, 1));
                return null;
            }

            @Nonnull
            @Override
            public CompletableFuture<Void> submit(boolean shouldQueue) {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Nonnull
    @Override
    public RestAction<Void> clearReactions() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<Void> clearReactions(@Nonnull String unicode) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<Void> clearReactions(@Nonnull Emote emote) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<Void> removeReaction(@Nonnull Emote emote) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<Void> removeReaction(@Nonnull Emote emote, @Nonnull User user) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<Void> removeReaction(@Nonnull String unicode) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<Void> removeReaction(@Nonnull String unicode, @Nonnull User user) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public ReactionPaginationAction retrieveReactionUsers(@Nonnull Emote emote) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public ReactionPaginationAction retrieveReactionUsers(@Nonnull String unicode) {
        throw new UnsupportedOperationException();
    }

    @Nullable
    @Override
    public MessageReaction.ReactionEmote getReactionByUnicode(@Nonnull String unicode) {
        throw new UnsupportedOperationException();
    }

    @Nullable
    @Override
    public MessageReaction.ReactionEmote getReactionById(@Nonnull String id) {
        throw new UnsupportedOperationException();
    }

    @Nullable
    @Override
    public MessageReaction.ReactionEmote getReactionById(long id) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public AuditableRestAction<Void> suppressEmbeds(boolean suppressed) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<Message> crosspost() {
        return null;
    }

    @Override
    public boolean isSuppressedEmbeds() {
        return false;
    }

    @Nonnull
    @Override
    public EnumSet<MessageFlag> getFlags() {
        throw new UnsupportedOperationException();
    }

    @Override
    public long getFlagsRaw() {
        return 0;
    }

    @Override
    public boolean isEphemeral() {
        return false;
    }

    @Nonnull
    @Override
    public MessageType getType() {
        throw new UnsupportedOperationException();
    }

    @org.jetbrains.annotations.Nullable
    @Override
    public Interaction getInteraction() {
        return null;
    }

    @Override
    public RestAction<ThreadChannel> createThreadChannel(String name) {
        return null;
    }

    @Override
    public void formatTo(Formatter formatter, int flags, int width, int precision) {

    }

    @Override
    public long getIdLong() {
        return 0;
    }

    @Override
    public String toString() {
        return "FM:\"" + content+ "\"";
    }
}
