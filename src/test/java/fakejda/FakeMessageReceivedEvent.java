package fakejda;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import javax.annotation.Nonnull;

public class FakeMessageReceivedEvent extends MessageReceivedEvent {
    private User user;
    public FakeMessageReceivedEvent(@Nonnull JDA api, long responseNumber, @Nonnull Message message) {
        super(api, responseNumber, message);
        this.user = message.getAuthor();
    }

    @Nonnull
    @Override
    public User getAuthor() {
        return user;
    }
}
