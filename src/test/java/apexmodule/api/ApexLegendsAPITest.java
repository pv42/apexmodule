package apexmodule.api;

import apexmodule.api.dto.Platform;
import apexmodule.api.dto.PlayerStats;
import disibot.util.Util;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ApexLegendsAPITest {

    private ApexLegendsAPI api;
    @BeforeEach
    void setUp() throws IOException {
        api = new ApexLegendsAPI(Util.readTokenFile("apex_api_key"));
    }

    @Test
    void testGetPlayerStatsByName() throws IOException, ApexAPIException {
        assertEquals("wur5t", api.getPlayerStats("wur5t" , Platform.PC).getPlayer().getName());
    }

    @Test
    void testGetPlayerStatsByID() throws IOException, ApexAPIException {
        assertEquals("wur5t", api.getPlayerStats(2305848418L , Platform.PC).getPlayer().getName());

    }

    @Test
    void testgetPlayersStatsByNames() throws IOException, ApexAPIException {
        ArrayList<String> names = new ArrayList<>();
        names.add("wur5t");
        names.add("Jekkt888");
        List<PlayerStats> stats = api.getPlayersStatsByNames(names, Platform.PC);
        assertEquals(2, stats.size());
        assertEquals("wur5t", stats.get(0).getName());
        assertEquals("Ledac", stats.get(1).getName());
    }

    @Test
    void testGetPlayersStats() throws IOException, ApexAPIException {
        List<ApexPlayer> list = new ArrayList<>();
        ApexPlayer player = new ApexPlayer("wur5t", 2305848418L, "-");
        list.add(player);
        player = new ApexPlayer("HeirloomOwner", 1008181415834L, "-");
        list.add(player);
        List<PlayerStats> stats =api.getPlayersStats(list , Platform.PC);
        assertEquals("wur5t", stats.get(0).getName());
        assertEquals("HeirloomOwner", stats.get(1).getName());
    }
}