package apexmodule;


import apexmodule.api.ApexLegendsAPI;
import disibot.command.AbstractCommandHandlerTester;
import disibot.context.ContextStorage;
import disibot.util.Command;
import disibot.util.Util;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ApexCommandHandlerTest extends AbstractCommandHandlerTester<ApexCommandHandler> {

    @BeforeEach
    void prepare() {
        ApexLegendsAPI api = null;
        try {
            api = new ApexLegendsAPI(Util.readTokenFile("apex_api_key"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        handler = new ApexCommandHandler(new ContextStorage(), api, null);
    }

    @Test
    void testCanHandle() {
        assertTrue(handler.canHandleCommand(new Command("apex rank wur5t"), null));
        assertTrue(handler.canHandleCommand(new Command("apex"), null));
        assertFalse(handler.canHandleCommand(new Command("rank wur5t"), null));
    }

    @Test
    void testRank() {
        // todo mock api response
        // sendCommand("apex rank wur5t");
        // assertLastMessageMatch("wur5t is currently at \\d{4} \\(Platinum 4\\)");
    }
}