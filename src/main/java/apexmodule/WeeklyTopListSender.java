package apexmodule;

import apexmodule.api.ApexAPIException;
import apexmodule.api.ApexLegendsAPI;
import apexmodule.api.ApexPlayer;
import apexmodule.api.RankedData;
import apexmodule.api.dto.Platform;
import apexmodule.api.dto.Rank;
import disibot.tasks.Task;
import disibot.util.SVGSender;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import org.apache.batik.transcoder.TranscoderException;
import org.jetbrains.annotations.Contract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Clock;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class WeeklyTopListSender {
    private static final Logger LOG = LoggerFactory.getLogger(WeeklyTopListSender.class);
    private final JDA jda;
    private final UserRankStorage storage;
    private final Clock clock;
    private ApexConfig config;
    private final ApexLegendsAPI api;


    public WeeklyTopListSender(@Nonnull UserRankStorage storage, JDA jda, ApexLegendsAPI api) {
        this.storage = storage;
        this.jda = jda;
        this.api = api;
        clock = Clock.systemDefaultZone();
    }

    @Nonnull
    @Contract(pure = true)
    private static String getArrowAngle(int compareValue) {
        if (compareValue < 0) return "45";  // /^
        if (compareValue > 0) return "135"; // \v
        return "90"; // ->
    }

    @Nonnull
    @Contract(pure = true)
    private static String getArrowColor(int compareValue) {
        if (compareValue < 0) return "#4bb44b"; // green
        if (compareValue > 0) return "#9c3213"; // red
        return "#aaaaaa"; //grey
    }

    @Nonnull
    @Contract("_ -> new")
    private static String loadSVGFile(String file) throws IOException {
        File svgFile = new File(file);
        FileInputStream in = new FileInputStream(svgFile);
        byte[] data = new byte[(int) svgFile.length()];
        int bytesRead = in.read(data);
        if (bytesRead != data.length) throw new IOException("Could not read whole file. (" + file + ")");
        return new String(data);
    }

    public Task createTask() {
        LocalDateTime phase = nextDayOfWeekWithHour(config.getWtlDay(), config.getWtlHour());
        return new Task("ApexWeeklyTopListSenderTask", Period.ofWeeks(1), phase,
                this::sendWeeklyRanks);
    }

    private LocalDateTime nextDayOfWeekWithHour(@Nonnull DayOfWeek dayOfWeek, int hour) {
        LocalDateTime now = LocalDateTime.now(clock);
        LocalDateTime dateTime = now.plus(Period.ofDays((7 + dayOfWeek.getValue() - now.getDayOfWeek().getValue()) % 7));
        if (now.getHour() < hour || now.getDayOfWeek() != dayOfWeek) {
            // if not on matching day and after time then subtract one extra week so the first deadline is less the 1
            // week in the future
            dateTime = dateTime.minus(Period.ofWeeks(1));
        }
        dateTime = dateTime.plus(Duration.ofHours(hour - dateTime.getHour())); // adjust hour
        dateTime = dateTime.minus(Duration.ofMinutes(dateTime.getMinute())); // set mins to 0
        dateTime = dateTime.minus(Duration.ofSeconds(dateTime.getSecond())); // set secs to 0
        return dateTime;
    }

    private void sendWeeklyRanks() {
        for (Guild guild : jda.getGuilds()) {
            sendWeeklySoloQRanking(guild);
        }
    }

    public void sendWeeklySoloQRanking(Guild guild) {
        try {
            String mainSvg = loadSVGFile("res/apex_weekly_ranking.svg");
            String partSvg = loadSVGFile("res/apex_weekly_ranking_player.svg");
            sendWeeklySoloQRankingVolatile(guild, mainSvg, partSvg);
        } catch (IOException e) {
            LOG.error("IOException while sending a soloQ ranking svg for server " + guild.getName() + ": " + e.getMessage());
        } catch (ApexAPIException e) {
            LOG.error("Failed to create weekly soloQ ranking svg for server " + guild.getName() + " because of a league api error: " + e.getMessage());
        } catch (TranscoderException e) {
            LOG.error("Failed to encode svg for soloQ ranking for server " + guild.getName() + ": " + e.getMessage());
        }
    }

    private void sendWeeklySoloQRankingVolatile(@Nonnull Guild guild, String mainSvgRaw, String partSvgRaw) throws TranscoderException, IOException, ApexAPIException {
        LOG.info("weekly soloQ ranking for " + guild.getName());
        String svgFormatted = formatSVG(mainSvgRaw, partSvgRaw, guild);
        MessageChannel channel = getWTLSendChannel(guild);
        if (channel != null) {
            SVGSender.send(svgFormatted, channel);
            LOG.info("weekly ranking for " + guild.getName() + " sent.");
        } else {
            LOG.info("Server " + guild.getName() + "does not seem to have a text channel with MESSAGE_WRITE permission, skipping emote podium");
        }
    }

    @Nullable
    private MessageChannel getWTLSendChannel(@Nonnull Guild guild) {
        MessageChannel channel = guild.getDefaultChannel();
        if(channel != null) return channel;
        LOG.info("Server " + guild.getName() + " does not have a default text channel, continue searching for valid channel.");
        for(TextChannel tc: guild.getTextChannels()) {
            Member member = guild.getMember(jda.getSelfUser());
            if(member==null) {
                LOG.warn("Bot was kicked from " + guild.getName() + " during wtl creation");
                continue;
            }
            if(member.hasPermission(tc, Permission.MESSAGE_SEND)) return tc;
        }
        return null;
    }

    @Nonnull
    private String formatSVG(String svg, String part, Guild guild) throws IOException, ApexAPIException {
        LocalDateTime now = LocalDateTime.now(clock);
        WeekFields weekFields = WeekFields.of(Locale.getDefault());
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.getDefault());
        svg = svg.replaceFirst("\\[week]", String.valueOf(now.get(weekFields.weekOfYear())));
        svg = svg.replaceFirst("\\[year]", String.valueOf(now.getYear()));
        svg = svg.replaceFirst("\\[end_date]", dateFormatter.format(now));
        svg = svg.replaceFirst("\\[entries]", formatPlayerSvgs(part, guild));
        svg = svg.replaceFirst("\\[start_date]", dateFormatter.format(now.minusWeeks(1)));
        return svg;
    }

    @Nonnull
    private String formatPlayerSvgs(@Nonnull String part, @Nonnull Guild guild) throws IOException, ApexAPIException {
        StringBuilder result = new StringBuilder();
        List<UserEntryData> entries = getUserEntryData(guild);
        for (int i = 0; i < entries.size(); i++) {
            result.append(formatPlayerSvg(part, entries.get(i), i));
        }
        storage.createSnapshot();
        return result.toString();
    }

    @Nonnull
    private List<UserEntryData> getUserEntryData(Guild guild) throws IOException, ApexAPIException {
        List<RankedData> oldQueueInfos = storage.getLastWeeksData();
        List<UserEntryData> entries = new ArrayList<>();
        for (User discordUser : storage.getUserPlayerMap().keySet()) {
            if (!guild.isMember(discordUser)) continue;
            ApexPlayer player = storage.getUserPlayerMap().get(discordUser);
            if (player == null) {
                LOG.warn("User " + discordUser.getName() + " has an apex player id assigned");
                continue;
            }
            Rank newRank = api.getPlayerStats(player.getUid(), Platform.PC).getRank();
            if (newRank == null) {
                LOG.warn("No ranking info found for player " + player.getName());
                continue;
            }
            Rank oldRank = null;
            for (RankedData queueInfo : oldQueueInfos) {
                if (queueInfo == null) continue;
                if (queueInfo.getPlayer().equals(player)) {
                    oldRank = queueInfo.getRank();
                    break;
                }
            }
            if (oldRank == null) {
                oldRank = Rank.createEmptyRank();
            }
            entries.add(new UserEntryData(player, discordUser, newRank, oldRank));
        }
        entries.sort((t0, t1) -> {
            int score0 = t0.newRank.getRankScore() - t0.oldRank.getRankScore();
            int score1 = t1.newRank.getRankScore() - t1.oldRank.getRankScore();
            return -Integer.compare(score0, score1);
        });
        return entries;
    }

    @Nonnull
    private String formatPlayerSvg(String format, @Nonnull UserEntryData user, int y_index) throws IOException, ApexAPIException {
        String entry = format;
        String oldImg = user.oldRank.getRankImageUrl();
        String newImg = user.newRank.getRankImageUrl();
        if(oldImg == null) oldImg = "https://storage.googleapis.com/dpss-wordpress-prod/2/2017/01/missing-person-icon.png";
        if(newImg == null) newImg = "https://storage.googleapis.com/dpss-wordpress-prod/2/2017/01/missing-person-icon.png";
        String playerAvatar = user.player.getAvatarUrl();
        if(playerAvatar == null) playerAvatar = "https://storage.googleapis.com/dpss-wordpress-prod/2/2017/01/missing-person-icon.png";
        //int wins = user.newQueueInfo.getWins() - user.oldQueueInfo.getWins();
        //int losses = user.newQueueInfo.getLosses() - user.oldQueueInfo.getLosses();
        entry = entry.replaceFirst("\\[y_offset]", String.valueOf(85 + 45 * y_index)); //85,130 (85+45)
        entry = entry.replaceFirst("\\[player_icon]", playerAvatar);
        entry = entry.replaceFirst("\\[player_name]", user.player.getName());
        entry = entry.replaceFirst("\\[discord_handle]", user.discordUser.getAsTag());
        //entry = entry.replaceFirst("\\[wins]", String.valueOf(wins));
        //entry = entry.replaceFirst("\\[losses]", String.valueOf(losses));
        //entry = entry.replaceFirst("\\[win_percent]", String.valueOf(Math.round(100f * (float) wins / (float) (wins + losses))));
        entry = entry.replaceFirst("\\[old_rank]", user.oldRank.getRankAndDivision());
        entry = entry.replaceFirst("\\[old_badge]", oldImg);
        entry = entry.replaceFirst("\\[old_score]", String.valueOf(user.oldRank.getRankScore()));
        entry = entry.replaceFirst("\\[arrow_rotation]", getArrowAngle(user.oldRank.compareTo(user.newRank)));
        entry = entry.replaceFirst("\\[arrow_color]", getArrowColor(user.oldRank.compareTo(user.newRank)));
        entry = entry.replaceFirst("\\[new_rank]", user.newRank.getRankAndDivision());
        entry = entry.replaceFirst("\\[new_badge]", newImg);
        entry = entry.replaceFirst("\\[new_score]", String.valueOf(user.newRank.getRankScore()));
        return entry;
    }

    public void setConfig(ApexConfig config) {
        this.config  = config;
    }

    private static class UserEntryData {
        ApexPlayer player; // only name and img are important
        User discordUser;
        Rank newRank;
        Rank oldRank;

        public UserEntryData(ApexPlayer player, User discordUser, Rank newRank, Rank oldRank) {
            this.player = player;
            this.discordUser = discordUser;
            this.newRank = newRank;
            this.oldRank = oldRank;
        }
    }
}

