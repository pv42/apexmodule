package apexmodule;

import apexmodule.api.ApexLegendsAPI;
import disibot.context.ContextStorage;
import disibot.modules.DisiBotModule;
import disibot.modules.DisiBotModuleInterface;
import disibot.util.Util;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.User;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class ApexModule extends DisiBotModule {
    private ApexCommandHandler commandHandler;
    private ApexLegendsAPI api;
    private final WeeklyTopListSender wtls;
    private final UserRankStorage userRankStorage;
    private ApexConfig config;

    public ApexModule(ContextStorage contextStorage, JDA jda) {
        try {
            api = new ApexLegendsAPI(Util.readTokenFile("apex_api_key"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        userRankStorage = UserRankStorage.fromDisk(api, jda);
        this.commandHandler = new ApexCommandHandler(contextStorage, api, userRankStorage);
        this.wtls = new WeeklyTopListSender(UserRankStorage.fromDisk(api,jda), jda, api);

    }

    @Override
    public void load(@NotNull DisiBotModuleInterface moduleInterface) {
        moduleInterface.addCommandHandler(this, commandHandler);
        config = new ApexConfig(moduleInterface.getConfig(this, ApexConfig::new));
        wtls.setConfig(config);
        moduleInterface.addTask(this, wtls.createTask());
    }
}
