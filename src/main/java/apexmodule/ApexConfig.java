package apexmodule;

import disibot.config.ConfigSection;

import java.time.DayOfWeek;

import static java.time.DayOfWeek.THURSDAY;

public class ApexConfig extends ConfigSection {

    private static final String WTL_HOUR = "apexWtlHour";
    private static final String WTL_DAY = "apexWtlDay";

    public ApexConfig() {
        super();
        setInt(WTL_DAY, THURSDAY.getValue());
        setInt(WTL_HOUR, 18);
    }

    public ApexConfig(ConfigSection config) {
        super(config);
    }

    public DayOfWeek getWtlDay() {
        return DayOfWeek.of(getInt(WTL_DAY));
    }

    public int getWtlHour() {
        return getInt(WTL_HOUR);
    }
}
