package apexmodule;

import apexmodule.api.ApexAPIException;
import apexmodule.api.ApexLegendsAPI;
import apexmodule.api.ApexPlayer;
import apexmodule.api.RankedData;
import apexmodule.api.dto.Platform;
import apexmodule.api.dto.PlayerStats;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import disibot.util.UserAdapter;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserRankStorage {
    private static final Logger LOG = LoggerFactory.getLogger(UserRankStorage.class);
    private static final String fileName = "apex_user_ranked_storage.json";
    private final List<PlayerRankSnapshot> snapshots;
    private final Map<User, ApexPlayer> userPlayerMap;
    private transient ApexLegendsAPI apexApi;
    private transient JDA jda;

    private UserRankStorage(ApexLegendsAPI apexAPI, JDA jda) {
        this.apexApi = apexAPI;
        this.jda = jda;
        userPlayerMap = new HashMap<>();
        snapshots = new ArrayList<>();
    }

    @Nonnull
    public static UserRankStorage fromDisk(ApexLegendsAPI apexApi, JDA jda) {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeHierarchyAdapter(User.class, new UserAdapter(jda));
        builder.enableComplexMapKeySerialization();
        Gson gson = builder.create();
        try {
            UserRankStorage storage = gson.fromJson(new FileReader(fileName), UserRankStorage.class);
            for (PlayerRankSnapshot snapshot : storage.snapshots) {
                if (snapshot != null) {
                    for (RankedData rank : snapshot.playerRankInfos) {
                        //if (rank != null) rank.setAPI((RiotAPIImpl) riotApi); // todo
                    }
                } else {
                    LOG.warn("Snapshot is null");
                }
            }
            storage.jda = jda;
            storage.apexApi = apexApi;
            return storage;
        } catch (FileNotFoundException e) {
            LOG.info("ranked storage does not exist yet");
            return new UserRankStorage(apexApi, jda);
        } catch (JsonSyntaxException e) {
            LOG.info("could not access api");
            return new UserRankStorage(apexApi, jda);
        } catch (NumberFormatException | NullPointerException e) {
            LOG.error("Could not read storage file: " + e.getMessage());
            e.printStackTrace();
            return new UserRankStorage(apexApi, jda);
        }
    }

    public void writeToDisk() {
        if (apexApi.isDisabled()) return;
        if (userPlayerMap.size() == 0 && snapshots.size() == 0) return;
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeHierarchyAdapter(User.class, new UserAdapter(jda));
        builder.enableComplexMapKeySerialization();
        Gson gson = builder.create();
        try {
            FileWriter fw = new FileWriter(fileName);
            gson.toJson(this, fw);
            fw.close();
        } catch (IOException e) {
            LOG.error("ranked storage could not be written to disk");
        }
    }

    @Nonnull
    public List<RankedData> getLastWeeksData() {
        LocalDateTime oneWeekAgo = LocalDateTime.now().minusWeeks(1);
        int beforeIndex = snapshots.size() - 1;
        if (beforeIndex == -1) {
            LOG.info("could not find previous snapshot");
            return new ArrayList<>(); // no data
        }
        while (beforeIndex > 0 && snapshots.get(beforeIndex).time.isAfter(oneWeekAgo)) {
            beforeIndex--;
        }
        int afterIndex = beforeIndex + 1;
        int index;
        if (afterIndex < snapshots.size()) {
            Duration beforeDuration = Duration.between(snapshots.get(beforeIndex).time, oneWeekAgo);
            Duration afterDuration = Duration.between(oneWeekAgo, snapshots.get(afterIndex).time);

            if (beforeDuration.getSeconds() < afterDuration.getSeconds()) {
                index = beforeIndex;
            } else {
                index = afterIndex;
            }
        } else {
            index = beforeIndex;
        }
        return snapshots.get(index).playerRankInfos;
    }

    public void createSnapshot() {
        PlayerRankSnapshot snapshot = new PlayerRankSnapshot();
        snapshot.playerRankInfos = new ArrayList<>();
        try {
            List<PlayerStats> stats = apexApi.getPlayersStats(userPlayerMap.values(), Platform.PC);
            for (PlayerStats stat : stats) {
                snapshot.playerRankInfos.add(new RankedData(
                        stat.getPlayer(),
                        stat.getRank()));
            }
        } catch (IOException | ApexAPIException e) {
            LOG.error("Could not create snapshot: " + e.getMessage());
            e.printStackTrace();
            return;
        }
        if(snapshot.playerRankInfos.size() == 0) {
            LOG.info("Did not create empty snapshot");
            return;
        }
        snapshot.time = LocalDateTime.now();
        LOG.info("Created snapshots with " + userPlayerMap.size() + " players");
        snapshots.add(snapshot);
        writeToDisk();
    }

    public Map<User, ApexPlayer> getUserPlayerMap() {
        return userPlayerMap;
    }

    public void putUsersPlayer(User user, ApexPlayer apexPlayer) {
        userPlayerMap.put(user, apexPlayer);
        createSnapshot();
    }

    public void removeUsers(User user) {
        userPlayerMap.remove(user);
        writeToDisk();
    }

    static class PlayerRankSnapshot {
        private List<RankedData> playerRankInfos;
        private LocalDateTime time;
    }
}

