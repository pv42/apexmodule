package apexmodule;

import apexmodule.api.ApexAPIException;
import apexmodule.api.ApexLegendsAPI;
import apexmodule.api.ApexPlayer;
import apexmodule.api.dto.Platform;
import apexmodule.api.dto.PlayerStats;
import apexmodule.api.dto.Rank;
import disibot.command.CommandHandler;
import disibot.context.ContextStorage;
import disibot.util.Command;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.Map;

public class ApexCommandHandler extends CommandHandler {
    private final ApexLegendsAPI api;
    private final UserRankStorage storage;

    public ApexCommandHandler(ContextStorage contexts, ApexLegendsAPI api, UserRankStorage storage) {
        super(contexts);
        this.api = api;
        this.storage = storage;
    }

    @Override
    public boolean canHandleCommand(@NotNull Command command, @NotNull MessageReceivedEvent event) {
        return command.peekString().equals("apex");
    }

    @Override
    protected void handle(@NotNull Command command) {
        String cmd = command.nextString();
        String subcmd = command.nextString();
        if (subcmd.equals("rank")) {
            if (api == null) {
                reply("Api is not initialized");
                return;
            }
            try {
                String playerName = getPlayerName(command);
                if (playerName == null) return;
                PlayerStats stats = api.getPlayerStats(playerName, Platform.PC);
                Rank rank = stats.getRank();
                reply(stats.getName() + " is currently at " + rank.getRankScore() + " (" + rank.getRankName() + " " + rank.getRankDivision() + ")");
            } catch (IOException e) {
                e.printStackTrace();
                reply("Could not get player stats");
            } catch (ApexAPIException e) {
                e.printStackTrace();
                reply("Apex APi error");
            }
        } else if (subcmd.equals("setname")) {
            String name = command.nextString();
            if (!name.equals("")) {
                try {
                    PlayerStats stats = api.getPlayerStats(name, Platform.PC);
                    storage.putUsersPlayer(getSenderAsUser(), stats.getPlayer());
                    reply("Set player name to '" + stats.getName() + "'");
                } catch (IOException e) {
                    reply("The player '" + name + "' could not be fetched.");
                } catch (ApexAPIException e) {
                    reply("Apex APi error");
                }
            } else {
                storage.removeUsers(getSenderAsUser());
                reply("Removed apex player name.");
            }
        } else {
            reply("Usage: \n" +
                    "`!apex rank <player name>`\n" +
                    "`!apex setname <player name>`");
        }
    }

    @Nullable
    private String getPlayerName(Command command) {
        if (!command.hasNext()) {
            User user = getSenderAsUser();
            Map<User, ApexPlayer> map = storage.getUserPlayerMap();
            if (map.containsKey(user)) return map.get(user).getName();
            reply("Please specify a player name or use !apex setname to save your own summoner name.");
            return null;
        } else {
            return command.nextString();
        }
    }
}
