package apexmodule.api;

import apexmodule.api.dto.Platform;
import apexmodule.api.dto.PlayerStats;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static java.net.HttpURLConnection.HTTP_OK;

public class ApexLegendsAPI {
    private static final Logger LOG = LoggerFactory.getLogger(ApexLegendsAPI.class);
    private final String apiKey;

    public ApexLegendsAPI(String apiKey) {
        this.apiKey = apiKey;
    }

    public PlayerStats getPlayerStats(String playerName, Platform platform) throws IOException, ApexAPIException {
        URL l = new URL("https://api.mozambiquehe.re/bridge?version=4&platform=" + platform +
                "&player=" + playerName + "&auth=" + apiKey);
        return download(l, PlayerStats.class);
    }

    public PlayerStats getPlayerStats(long uid, Platform platform) throws IOException, ApexAPIException {
        URL l = new URL("https://api.mozambiquehe.re/bridge?version=4&platform=" + platform +
                "&uid=" + uid + "&auth=" + apiKey);
        return download(l, PlayerStats.class);
    }

    public List<PlayerStats> getPlayersStatsByNames(Collection<String> playerNames, Platform platform) throws IOException, ApexAPIException {
        List<PlayerStats> playerStats = new ArrayList<>();
        for(String playerName: playerNames) {
            playerStats.add(getPlayerStats(playerName, platform));
        }
        return playerStats;
        /*StringBuilder namesStringBuilder = new StringBuilder();
        for (String name : playerNames) {
            namesStringBuilder.append(name).append(",");
        }
        String namesString = namesStringBuilder.substring(0, namesStringBuilder.length() - 1);
        URL url = new URL("https://api.mozambiquehe.re/bridge?version=4&platform=" + platform +
                "&player=" + namesString + "&auth=" + apiKey);
        TypeToken<List<PlayerStats>> typeToken = new TypeToken<>() {};
        if(playerNames.size() == 1) return Collections.singletonList(download(url, PlayerStats.class));
        else return download(url, typeToken.getType());*/
    }

    public List<PlayerStats> getPlayersStats(Collection<ApexPlayer> players, Platform platform) throws IOException, ApexAPIException {
        List<PlayerStats> playerStats = new ArrayList<>();
        for(ApexPlayer player: players) {
            playerStats.add(getPlayerStats(player.getUid(), platform));
        }
        return playerStats;
        /*StringBuilder namesStringBuilder = new StringBuilder();
        for (ApexPlayer player : players) {
            namesStringBuilder.append(player.getUid()).append(",");
        }
        String uidsString = namesStringBuilder.substring(0, namesStringBuilder.length() - 1);
        URL url = new URL("https://api.mozambiquehe.re/bridge?version=4&platform=" + platform +
                "&uid=" + uidsString + "&auth=" + apiKey);
        TypeToken<List<PlayerStats>> typeToken = new TypeToken<>() {};
        if(players.size() == 1) return Collections.singletonList(download(url, PlayerStats.class));
        else return download(url, typeToken.getType());*/
    }

    private <T> T download(URL url, Type type) throws IOException, ApexAPIException {
        LOG.info("downloading " + url.toString().split("&auth=")[0]);  // remove api key from log
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        if (connection.getResponseCode() != HTTP_OK) {
            String response = new BufferedReader(new InputStreamReader(connection.getErrorStream())).lines().collect(Collectors.joining("\n"));
            throw new ApexAPIException("Unexpected return code " + connection.getResponseCode() + ": " + response);
        }
        return new Gson().fromJson(new InputStreamReader(connection.getInputStream()), type);
    }

    private <T> T download(URL url, Class<T> clazz) throws IOException, ApexAPIException {
        LOG.info("downloading " + url.toString().split("&auth=")[0]);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        if (connection.getResponseCode() != HTTP_OK) {
            String response = new BufferedReader(new InputStreamReader(connection.getErrorStream())).lines().collect(Collectors.joining("\n"));
            throw new ApexAPIException("Unexpected return code " + connection.getResponseCode() + ": " + response);
        }
        return new Gson().fromJson(new InputStreamReader(connection.getInputStream()), clazz);
    }

    public boolean isDisabled() {
        return false;
    }
}
