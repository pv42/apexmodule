package apexmodule.api;

public class ApexPlayer {
    private final String name;
    private final long uid;
    private final String avatar;

    public ApexPlayer(String name, long uid, String avatar) {
        this.name = name;
        this.uid = uid;
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public long getUid() {
        return uid;
    }

    public String getAvatarUrl() {
        return avatar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApexPlayer that = (ApexPlayer) o;
        return uid == that.uid;
    }

    @Override
    public int hashCode() {
        return (int) (uid ^ (uid >>> 32));
    }
}
