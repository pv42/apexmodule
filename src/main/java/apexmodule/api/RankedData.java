package apexmodule.api;

import apexmodule.api.dto.Rank;

public class RankedData {
    private final ApexPlayer player;
    private final Rank rank;

    public RankedData(ApexPlayer player, Rank rank) {
        this.player = player;
        this.rank = rank;
    }

    public ApexPlayer getPlayer() {
        return player;
    }

    public Rank getRank() {
        return rank;
    }
}
