package apexmodule.api;

public class ApexAPIException extends Exception {
    public ApexAPIException(String msg) {
        super(msg);
    }
}
