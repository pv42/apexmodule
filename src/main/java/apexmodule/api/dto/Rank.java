package apexmodule.api.dto;

import javax.annotation.Nonnull;

public class Rank implements Comparable<Rank> {
    private int ladderPos;
    private int rankDiv;
    private String rankImg;
    private String rankName;
    private int rankScore;
    private String rankedSeason;

    public static Rank createEmptyRank() {
        return new Rank();
    }

    public int getRankScore() {
        return rankScore;
    }

    public String getRankImageUrl() {
        return rankImg;
    }

    public String getRankName() {
        return rankName;
    }

    public int getRankDivision() {
        return rankDiv;
    }

    public String getRankedSeason() {
        return rankedSeason;
    }

    public String getRankAndDivision() {
        return rankName + " " + rankDiv;
    }

    @Override
    public int compareTo(@Nonnull Rank other) {
        return compare(this, other);
    }

    public static int compare(@Nonnull Rank first, @Nonnull Rank second) {
        return Integer.compare(first.getRankScore(), second.getRankScore());
    }


}
