package apexmodule.api.dto;

public class ApexLegendData {
    private ImgAssets ImgAssets;

    public String getIconUrl() {
        return ImgAssets.icon;
    }

    public String getBannerUrl() {
        return ImgAssets.banner;
    }

    private static class ImgAssets {
        private String icon;
        private String banner;
    }
}
