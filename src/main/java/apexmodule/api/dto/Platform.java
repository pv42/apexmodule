package apexmodule.api.dto;

public enum Platform {
    PC,
    PS4,
    X1
}
