package apexmodule.api.dto;

import apexmodule.api.ApexPlayer;

import java.util.Map;

public class PlayerStats {
    private Global global;
    private Realtime realtime;
    private Legends legends;
    private MozambiquehereInternal mozambiquehere_internal;

    public ApexPlayer getPlayer() {
        return new ApexPlayer(global.name, global.uid, global.avatar);
    }

    public Rank getRank() {
        return global.rank;
    }

    public String getName() {
        return global.name;
    }

    public int getLevel() {
        return global.level;
    }

    public String getAvatarUrl() {
        return global.avatar;
    }

    public boolean isBanned() {
        return global.bans.isActive;
    }

    public long remainingBanTimeSeconds() {
        return global.bans.remainingSeconds;
    }

    public int getToNextLevelPercent() {
        return global.toNextLevelPercent;
    }

    public int getCurrentBattlePassLevel() {
        return global.battlePass.level;
    }

    public int getBattlePassLevel(int season) {
        switch (season) {
            case 1: return global.battlePass.history.season1;
            case 2: return global.battlePass.history.season2;
            case 3: return global.battlePass.history.season3;
            case 4: return global.battlePass.history.season4;
            case 5: return global.battlePass.history.season5;
            case 6: return global.battlePass.history.season6;
            case 7: return global.battlePass.history.season7;
            case 8: return global.battlePass.history.season8;
            default: return -1;
        }
    }

    public long getUID() {
        return global.uid;
    }


    private static class Global {
        String name;
        String avatar;
        Bans bans;
        BattlePass battlePass;
        long internalUpdateCount;
        int level;
        int toNextLevelPercent;
        long uid;
        Rank rank;
        private static class Bans {
            boolean isActive;
            String last_banReason;
            long remainingSeconds;
        }

        private static class BattlePass {
            int level;
            History history;

            private static class History {
                int season1;
                int season2;
                int season3;
                int season4;
                int season5;
                int season6;
                int season7;
                int season8;
            }
        }
    }

    private static class Realtime {
        String lobbyState; // eg. open
        int isOnline; // the next seem to be disguised booleans
        int isInGame;
        int canJoin;
        int partyFull;
        String selectedLegend;
    }

    private static class Legends {
        Map<String, ApexLegendData> all;
    }

    private class MozambiquehereInternal {
        boolean isNewToDB;
        String claimedBy; // may be int
        String APIAccessType; // e.g. BASIC
        String ClusterID; // may be int
        RateLimit rate_limit;


    }

    private static class RateLimit {
        int max_per_second;
        int current_req; // may be int
    }
}
